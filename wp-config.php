<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'swe-bck');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-BsDH<)~6Hb8(#xJ}M9Gb9B-9|@5z8]t9}tCq7n3rm}C#Q-tbpszjpC5;PBr(OVB');
define('SECURE_AUTH_KEY',  'k`>b11xnQg6&)-U@0lz;5k/LvzOQ:J_QxX+0p,_.WAJo4.^b1!Fn5C;VxOEMW[6%');
define('LOGGED_IN_KEY',    '=*frP}w{UU[{X.>QPJn!HL%^(}||D1oi*]e!uD+_WNE<`5ODUvxxqD$XIr-z,T!9');
define('NONCE_KEY',        ')9B#P6.>/,zuh!eHu^sULA%0CYS*.Ex@tOULum]4F-)i^QoRiR,;S}7x2EsW{WSY');
define('AUTH_SALT',        'z=x7nD.DN*zKeWHnZDu5U<3Ab?7Ub;}B9]us7u<CT5(&M9_1(k_@ucfD)kb<SjX-');
define('SECURE_AUTH_SALT', '(r*X ?.4y2;biDQ|^^qg8S56!)0xsc%AOPLBzY2JX92@QCdX.`rSEit8!G+;e_F[');
define('LOGGED_IN_SALT',   'jDi|4V!r+;SH]J*AT^Xf 56UZv2hyskU/9?%rVDyQz}qmoDx)E.!B49+BwKi54[#');
define('NONCE_SALT',       'VJu6<[0QN8u|P[}(GwRLWxKa0REqS~}YNV&%tW vI896mKpn;ck3<W?,BMKYF&g%');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

define('FTP_HOST', 'localhost');
define('FTP_USER', 'daemon');
define('FTP_PASS', 'xampp');

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
