<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];


//========== CUSTOM SCRIPTS ==========
define("URLTEMA", get_bloginfo("template_url"));
define("URLTEMARAIZ", str_replace("/loja", "", get_bloginfo("template_url")) . "/assets/");

function getHtml($url, $post = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, get_template_directory_uri() . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    if(!empty($post)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    } 
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


$data = [
    'name' => 'Color',
    'slug' => 'pa_color',
    'type' => 'select',
    'order_by' => 'menu_order',
    'has_archives' => true
];

//print_r($woocommerce->post('products/attributes', $data));

//========== CUSTOM SCRIPTS ==========

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */

function posttype_tabelas() 
{    
    $labels = array(
		'name'                => ( 'Tabelas'),
		'singular_name'       => ( 'Tabela'),
		'menu_name'           => ( 'Tabelas'),
		'parent_item_colon'   => ( 'Parent Tabelas'),
		'all_items'           => ( 'Todas as Tabelas'),
		'view_item'           => ( 'Visualizar Tabela'),
		'add_new_item'        => ( 'Adicionar Nova Tabela'),
		'add_new'             => ( 'Adicionar Tabela'),
		'edit_item'           => ( 'Editar Tabela'),
		'update_item'         => ( 'Atualizar Tabela'),
		'search_items'        => ( 'Pesquisar Tabela'),
		'not_found'           => ( 'Tabela não encontrada'),
		'not_found_in_trash'  => ( 'Nenhuma Tabela encontrada na lixeira')
            );
    
    register_post_type( 'post_tabelas',
        array(
			'show_ui' => true,
            'menu_icon'   => 'dashicons-media-spreadsheet',
            'labels'      => $labels,
            'public'      => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
            'has_archive' => true,
			'hierarchical' => false,
            'supports'    => array( 'title')
        )
    );
}

function posttype_certificados() 
{    
    $labels = array(
		'name'                => ( 'Certificados'),
		'singular_name'       => ( 'Certificado'),
		'menu_name'           => ( 'Certificados'),
		'parent_item_colon'   => ( 'Parent Certificados'),
		'all_items'           => ( 'Todos os Certificados'),
		'view_item'           => ( 'Visualizar Certificado'),
		'add_new_item'        => ( 'Adicionar Novo Certificado'),
		'add_new'             => ( 'Adicionar Certificado'),
		'edit_item'           => ( 'Editar Certificado'),
		'update_item'         => ( 'Atualizar Certificado'),
		'search_items'        => ( 'Pesquisar Certificado'),
		'not_found'           => ( 'Certificado não encontrado'),
		'not_found_in_trash'  => ( 'Nenhum Certificado encontrado na lixeira')
            );
    
    register_post_type( 'post_certificados',
        array(
			'show_ui' => true,
            'menu_icon'   => 'dashicons-awards',
            'labels'      => $labels,
            'public'      => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
            'has_archive' => true,
			'hierarchical' => false,
            'supports'    => array( 'title')
        )
    );
}

add_action('init', 'posttype_certificados');
add_action('init', 'posttype_tabelas');
