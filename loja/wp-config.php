<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'swe-site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'w-lJja+WPLC)bf{4ad(_[LRcQRx~VS}  [j<3;yxpSR@Nn5cj0,X>Q#gJA_t-Qle' );
define( 'SECURE_AUTH_KEY',  'BHI;A1gfAY>~F2aW!)pzD[gHq6s&ZgV6VvvYB|Lz}}EvcG~7npjWyw=h,:^B6+H.' );
define( 'LOGGED_IN_KEY',    '3mqSPmP,wRdtl>D|0Zy2;hhi*Sh70<rz&7Lb}+,Hd!<eQKwSY^-@[nd:@&x<fQ?I' );
define( 'NONCE_KEY',        'UXgJeD>EEeD., FcZB~_E4lV]{1zBJU{ig0*=YOQyCIz|dx}-=FbM3/eT5`,b}2x' );
define( 'AUTH_SALT',        '[.A#aU~@p9jMI#M[V_(@Zls)Ek3!7gG2)>*aQ]BffP 7609@ZI/od<D>Kl25_zs}' );
define( 'SECURE_AUTH_SALT', 'DLbFy|,hb1XFn?8aB9:<PF;Kje1r`Ea-{aS]skC`Pro9a{t,0T{I6PtD+S3$?Go<' );
define( 'LOGGED_IN_SALT',   'E`FJ;CSeGK^2upX/B5rDOuxiee/-Tm3OqZ-7SwyCog658m8huRF fgd9!`CSa`1:' );
define( 'NONCE_SALT',       's{kt);P#lS8DC#3}WU;C1gQLQ.TI0? GPLEU{<Mj,I]xZd|D9Ax(oX.2hes(,xZE' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'sweloja_';

define('FTP_HOST', 'localhost');
define('FTP_USER', 'daemon');
define('FTP_PASS', 'xampp');

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

define('FTP_HOST', 'localhost');
define('FTP_USER', 'daemon');
define('FTP_PASS', 'xampp');

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
