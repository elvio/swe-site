<?php
function setup(){

//limpa do wp_head removendo tags desnecessárias
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');
remove_action('wp_head', 'wp_shortlink_wp_head');

//remove smart quotes
remove_filter('the_title', 'wptexturize');
remove_filter('the_content', 'wptexturize');
remove_filter('the_excerpt', 'wptexturize');
remove_filter('comment_text', 'wptexturize');
remove_filter('list_cats', 'wptexturize');
remove_filter('single_post_title', 'wptexturize');

function remove_menus(){
  
}

add_action('acf/init', 'my_acf_init');

function my_acf_init() {
 
    if( function_exists('acf_add_options_page') ) {
        
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Opções do Tema', 'my_text_domain'),
            'menu_title'    => __('Opções do Tema', 'my_text_domain'),
            'menu_slug'     => 'opcoes',
        ));
    }
}


function whatsPage(){
	$page = "";
	if (is_front_page()) $page = "home";

	if (is_page("simulador-energia-solar") || is_page("sua-usina-solar-swe") || is_page("proposta-financiamento")) $page = "greenBd";

	return $page;
}

// ================= VERIFICA O TIPO DE PAGINA
function get_whatsPage($slug){
	$page = false;
	if (is_page($slug)) $page = true;

	return $page;
}

// ================= CUSTOM FIELD
if (function_exists('acf_add_options_page'))
{
	acf_add_options_page();
}

// ================= CONFIGURACOES PADRAO
add_action( 'admin_menu', 'remove_menus' );
	register_nav_menu('header-menu',__( 'Menu Principal' ));
	
	if ( function_exists( 'add_theme_support' ) ) :
		add_theme_support('post-thumbnails');
	endif;
	
	if ( function_exists( 'add_image_size' ) ) { 
	
	}	
}
add_action( 'init', 'setup' );

define("URLTEMA", get_bloginfo("template_url"));
define("SITEURL", get_bloginfo("url") );



// ================= BUSCA PRODUTOS SUGERIDOS
function relatedProducts() {
  $produtos = $_POST['sku'];
  
  $pos = 0;
  
  foreach ($produtos as $key => $value) {
  	$str .= "'" . $value[0] . "',";
  	if ($value[1]==1) $recomendado = $produtos[$pos][0];
  	$pos++;
  }
  
  $str = substr($str, 0,-1);
  
  global $wpdb;
  
  $prefix = "sweloja";
  $posttable = $prefix . '_posts';
  $postmeta =  $prefix . '_postmeta';

  $select = "SELECT
    p.ID,
	p.post_title,
    p.post_type,
    p.guid,
    m1.meta_value AS SKU,
    SUM(m2.meta_value) AS PRECO,
    m3.meta_value as ATTR
FROM $posttable p
JOIN $postmeta m1 ON (m1.post_id = p.ID AND m1.meta_key = '_sku')
JOIN $postmeta m2 ON (m2.post_id = p.ID AND m2.meta_key = '_price')
JOIN $postmeta m3 ON (m3.post_id = p.ID AND m3.meta_key = '_product_attributes')
WHERE m1.meta_value in ($str) && p.post_type= 'product' GROUP BY p.ID ORDER BY PRECO ASC";


  $number_count = $wpdb->get_results( $select );
  //"SELECT * FROM $produtos_table WHERE sku in ($str) ORDER BY max_price ASC"

  foreach ($number_count as $key => $value) {
  	$orderArr = array_push($orderArr, [$value->max_price, $value->product_id]);
  }
 
  $i=0;
  
  foreach ($number_count as $key => $value) {
  	//$number_post = $wpdb->get_results( "SELECT post_title, guid FROM $produtos_post WHERE ID=$value->ID LIMIT 1" );

  	$number_image = $wpdb->get_results( "SELECT guid FROM $posttable WHERE post_parent=$value->ID LIMIT 1" );

  	$ativo = ($value->SKU == $recomendado) ? "ativo" : '';
  	$label = ($value->SKU == $recomendado) ? "Recomendado pela SWE" : 'Escolher esse';
  	$chk = ($value->SKU == $recomendado) ? "checked" : '';
  	
  	$attr = (isset($value->ATTR)) ? unserialize($value->ATTR) : array('fabricante-modulo'=>'', 'fabricante-inversor'=>'', 'potencia-modulo'=>'', 'potencia-inversor'=>'');

	$html .= '<div class="col-4 '. $ativo .' ">';
	$preco = number_format($value->PRECO, 2, ',', '.');
	
	$html .= '<div class="aba"><label><input type="hidden" name="is_sku" value="' . $value->SKU . '"><input type="radio" name="rdEquipamento" value="'. $value->PRECO .'" '.$chk.'>'.$label.'</label></div>';

	$html .= '<input type="hidden" name="is_fabricante_modulo" value="'. $attr['fabricante-modulo']['value'] .'">';
	$html .= '<input type="hidden" name="is_fabricante_inversor" value="'. $attr['fabricante-inversor']['value'] .'">';
	$html .= '<input type="hidden" name="is_potencia_modulo" value="'. $attr['potencia-modulo']['value'] .'">';
	$html .= '<input type="hidden" name="is_potencia_inversor" value="'. $attr['potencia-inversor']['value'] .'">';
	
	
	$html .= '<div class="ctn">';
	$html .= '<img src="'.  $number_image[0]->guid .'" class="imgProduto">';
	$html .= "<h2>" . $value->post_title . "</h2>";
	$html .= "<p> R$ " . $preco . "</p>";
	$html .= '<a href="' . $value->guid . '" target="_blank" class="button btProposal">COMPRAR</a>';
	$html .= "</div>";
	$html .= "</div>";
    $i++;

    $return = array('success' => 1, 'html' => $html);
  }

  if ($i == 0){
    $html .= 'Não há equipamentos sugeridos.';
    $return = array('success' => 0, 'html' => $html, 'preco' => '0');
  }
  $output = json_encode($return);
  echo $output;

  
  //var_dump($return);
  
  wp_die();
}

add_action('wp_ajax_relatedProducts', 'relatedProducts');
add_action('wp_ajax_nopriv_relatedProducts', 'relatedProducts');

//=====================================================
// </ BUSCA PRODUTOS SUGERIDOS >
//=====================================================

function getHtml($url, $post = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, get_template_directory_uri() . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    if(!empty($post)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    } 
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


function posttype_depoimentos() 
{    
    $labels = array(
		'name'                => ( 'Depoimentos'),
		'singular_name'       => ( 'Depoimento'),
		'menu_name'           => ( 'Depoimentos'),
		'parent_item_colon'   => ( 'Parent Depoimentos'),
		'all_items'           => ( 'Todos os Depoimentos'),
		'view_item'           => ( 'Visualizar Depoimento'),
		'add_new_item'        => ( 'Adicionar Novo Depoimento'),
		'add_new'             => ( 'Adicionar Depoimento'),
		'edit_item'           => ( 'Editar Depoimento'),
		'update_item'         => ( 'Atualizar Depoimento'),
		'search_items'        => ( 'Pesquisar Depoimento'),
		'not_found'           => ( 'Depoimento não encontrado'),
		'not_found_in_trash'  => ( 'Nenhum Depoimento encontrado na lixeira')
            );
    
    register_post_type( 'post_depoimentos',
        array(
			'show_ui' => true,
            'menu_icon'   => 'dashicons-testimonial',
            'labels'      => $labels,
            'public'      => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
            'has_archive' => true,
			'hierarchical' => false,
            'supports'    => array( 'title', 'editor')
        )
    );
}

add_action('init', 'posttype_depoimentos');
add_action('wp_footer', 'footerJS' );

//CONTACT FORM 7

// ================= BUSCA DE COORDENADAS DE LOCALIZACAO

$gradekWh = array(
				[82.7,85.8,91.5,87.7,88.6,81.8,84.9,92.7,94.4,94.3,76.4,77.9],
				[74.2,66.7,75.8,74.1,94.2,96.2,111.1,114.2,99,103.5,92.7,86.1],
				[91.2,81.7,84.3,81.9,98.4,96.1,113.3,119.5,111.3,115.2,105.9,102.7],
				[110.7,99,108,94.6,92.7,81.1,85.1,106.7,104.3,111.8,116.4,111.2],
				[107.6,97.7,101.8,82.2,82.2,73.5,84.2,101.4,93.2,101.3,105.6,109.2],
				[101.6,96.7,104.9,103.3,96.5,94.3,100.7,111.5,109.6,108,95.6,93.1],
				[80.6,82.2,77.9,72.4,72.2,66.5,73.4,79,72.5,75.9,86.9,82.1],
				[91.1,79.6,87.3,78.7,79.3,73.7,78.6,83.1,84.7,90.8,88.5,90.3],
				[97.4,88.6,94.6,85.5,78,68.5,67.8,77.2,81,91.2,100.2,103],
				[114.7,98.6,102.3,90.4,78.6,65.8,68.1,73.8,85.2,102.1,112,117.8]
			);

function getMediaKwp($LAT){
	$lat = floatval($LAT);
	$coordenadas = array(
		["inicio"=>5, "fim"=>1, "media"=>85.583],
		["inicio"=>1, "fim"=>-3, "media"=>90.65],
		["inicio"=>-3, "fim"=>-7, "media"=>100.125],
		["inicio"=>-7, "fim"=>-11, "media"=>101.8],
		["inicio"=>-11, "fim"=>-15, "media"=>94.991],
		["inicio"=>-15, "fim"=>-20, "media"=>101.316],
		["inicio"=>-20, "fim"=>-24, "media"=>76.8],
		["inicio"=>-24, "fim"=>-28, "media"=>83.808],
		["inicio"=>-28, "fim"=>-32, "media"=>86.083],
		["inicio"=>-32, "fim"=>-36, "media"=>92.45]
	);
	$i=0;
	foreach ($coordenadas as $key => $value) 
	{
		if($lat <= $value["inicio"] && $lat > $value["fim"]){
			$media = array($value["media"], $i);
			break;
		}
		$i++;
	}
	return $media;
}

function checkCoords($UF, $CIDADE){
	global $wpdb;
  
	$prefix = $wpdb->prefix;
	$table = $prefix . 'coordenadas';

	$select = "SELECT * FROM $table WHERE UF='$UF' and MUNICIPIO LIKE '%$CIDADE%' LIMIT 1";

	$search = $wpdb->get_results( $select );
	$coords = [];
	foreach ($search as $key => $value) {
		$coords = [$value->LATITUDE];
	}
	
	return $coords;
}

add_filter( 'wpcf7_validate_text*', 'custom_text_validation_filter', 1, 2 );
add_theme_support( 'title-tag' );
  
function custom_text_validation_filter( $result, $tag ) {
	session_start();
	$tag = new WPCF7_Shortcode($tag);
	$result = (object)$result;

	$name = 'fobCalcCidade';

  if ( $name == $tag->name) {
  	
    $cidade = isset( $_POST['fobCalcCidade'] ) ? trim( wp_unslash( (string) $_POST['fobCalcCidade'] ) ) : '';
    $uf = isset( $_POST['estados'] ) ? trim( wp_unslash( (string) $_POST['estados'] ) ) : '';
    $coords = checkCoords($uf, $cidade);

	$_SESSION["COORDS"] = $coords ;
	$_SESSION["CONSUMO"] = $_POST['consumo'];
	$_SESSION["INSTALACAO"] = $_POST['rd-install']; 
   
    if ( count($coords) == 0) {
    	$result->invalidate( $tag, "Por favor, verifique a CIDADE e UF informados" );
    }
  }

  return $result;
}

// add_action('wpcf7_mail_sent', function ($cf7) {
// 	session_start();

// 	$wpcf = WPCF7_ContactForm::get_current();
	
// 	if ($wpcf->id == '28' || $wpcf->id == '361'){
		
// 	}
    
// });

add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
 
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
    $my_attr = array('chart', 
			'kwp', 
			'modulos',
			'consumo',
			'kwanual',
			'kwmedio',
			'tarifa',
			'economia',
			'roi',
			'roi_anos',
			'tabela_economia',
			'chart_economia',
			'sku',
			'valor_equipamento',
			'valor_instalacao',
			'total',
			'potencia_modulo',
			'potencia_inversor',
			'fabricante_modulo',
			'fabricante_inversor');
  

    foreach ($my_attr as $attr){
    	$out[$attr] = $atts[$attr];
    }
 
    return $out;
}

function footerJS()
{
?>

	<script>
		//TROCAR POR SEND wpcf7mailsent
		document.addEventListener( 'wpcf7mailsent', function( event ) {
	    var inputs = event.detail.inputs;
	 
	 	if ( '28' == event.detail.contactFormId || '361' == event.detail.contactFormId) {
		    for ( var i = 0; i < inputs.length; i++ ) {
		        if ( 'nome' == inputs[i].name ) {
		        	setCookie("nome", inputs[i].value, 1);
		        }

		        if ( 'telefone' == inputs[i].name ) {
		            setCookie("telefone", inputs[i].value, 1);
		        }

		        if ( 'email' == inputs[i].name ) {
		            setCookie("email", inputs[i].value, 1);
		        }
		        if ( 'estados' == inputs[i].name ) {
		            setCookie("estado", inputs[i].value, 1);
		        }

		        if ( 'fobCalcCidade' == inputs[i].name ) {
		            setCookie("cidade", inputs[i].value, 1);
		        }
		        // if ( 'consumo' == inputs[i].name ) {
		        //     setCookie("consumo", inputs[i].value, 1);
		        // }
		        if ( 'rd-install' == inputs[i].name ) {
		            setCookie("tipolocal", inputs[i].value, 1);
		        }

		    }

		    if(i == inputs.length) location = '<?php echo SITEURL ?>/sua-usina-solar-swe';
		}

	}, false );
	</script>

<?php 
}
?>

