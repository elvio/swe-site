<?php get_header();?>

<header id="header" class="pageSimuladorHeader">

	<?php require_once "inc/topo.php" ?>

	<ul class="steps">
		<li class="alpha"><span>1</span><label>Simulador solar</label></li>
		<li><span>2</span><label>Sua Usina Solar SWE</label></li>
		<li><span>3</span><label>Solicitar Proposta para Financiamento</label></li>
	</ul>

	<h2><div><img src="<?php echo URLTEMA ?>/images/icoSimulador.png"></div>Simulador Solar</h2>
	
</header><!-- /header -->

<main id="simulador">
	<section class="form">
		<div class="ctn">
			<?php echo do_shortcode('[contact-form-7 id="28" title="Simulador"]'); ?>
			<br clear="all">
		</div>
	</section>
</main>

<?php get_footer(); ?>