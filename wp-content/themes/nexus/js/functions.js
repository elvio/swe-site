// JavaScript Document
var didScroll=false;
var home = false;

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function formatMoney(n, c, d, t) {
  var c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;

  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};




var menu = {
	init:function(){

		$('.btProposal').click(function(event) {
			$('.modal-proposta').fadeIn(300, function() {
				
			});
		});

		$('.btContato').click(function(event) {
			$('.modal-contato').fadeIn(300, function() {
				
			});
		});

		$('.close', '.modal').click(function(event) {
			$(this).parents('.modal').fadeOut('fast', function() {
				
			});
		});

		$('.menuMobile').click(function(event) {
			$('.navMobile').fadeIn(300, function() {
				$('.close', this).show('fast', function() {
					
				});
			});
		});

		$('.close', '.navMobile').click(function(event) {
			$('.navMobile').fadeOut( 'fast', function() {
				
			});
		});

	}
}

var appHome = {
	init:function(){
		
		$('.itens ul').slick();
	}
}

var appSobre = {
	init:function() {
		$('.slider').slick({
		  centerMode: true,
		  centerPadding: '0px',
		  dots: true,
		  slidesToShow: 3,
		  variableWidth: true,
		  arrows: false,
		  responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '40px',
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        arrows: false,
		        centerMode: true,
		        centerPadding: '40px',
		        slidesToShow: 1
		      }
		    }
		  ]
		});


		$( ".depoimentos .person " ).click(function(event) {
			$parent = $(this);
			$( this ).toggleClass('active');
			//$( ".ask", $parent).toggleClass('arrowUp');
			$( ".text", this).slideToggle( "fast", function() {
				
		  });
		});

	}
}

var appSimulador = {
	init:function() {
		$('input[type="tel"]').mask('(99) 99999-9999');
	}
}


var CpfCnpjMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
		},
    cpfCnpjpOptions = {
    	onKeyPress: function(val, e, field, options) {
      	field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
      }
    };

$(function() {
	
})

var appFinanciamento = {
	init:function() {
		$cpf = $('#cpf');
		$('input[type="tel"]').mask('(99) 99999-9999');
		//$('input[name="cpf"]').mask('999.999.999-99');
		$('input[name="CEP"]').mask('99999-999');
		$cpf.mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);


		var nome = $('input[name="nome"]');
		var cidade = $('input[name="cidade"]');
		var estado = $('input[name="<estados></estados>"]');
		var telefone = $('input[name="telefone"]');
		var email = $('input[name="email"]');
		//var consumo = $('input[name="consumo"]');
		//var sku = $('input[name="sku"]');
		//var chart = $('input[name="chart"]');
		//var kwanual = $('input[name="kwanual"]');

		nome.val(getCookie("nome"));
		cidade.val(getCookie("cidade"));
		telefone.val(getCookie("telefone"));
		email.val(getCookie("email"));
		//sku.val(getCookie("sku"));
		//consumo.val(getCookie("consumo"));
		
		//kwanual.val(getCookie("kwanual"));
		

		$('select>option[value="' + getCookie("estado") + '"]').prop('selected', true);
		$('input:radio[value="' + getCookie("tipolocal") + '"]').prop('checked', true);
	}
}

var appSuaUsina = {
	init:function() {
		//alert(getCookie("nome"));
	}
}

function isMob() { 
 if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)

 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 || navigator.userAgent.match(/Windows Phone/i)
 ){
    return true;
  }
 else {
    return false;
  }
}

$(document).ready(function(e) {
	
	menu.init();
	
	if($('#home').length > 0) appHome.init();
	if ($('#sobre').length > 0) appSobre.init();
	if ($('#simulador').length > 0) appSimulador.init();
	if ($('#suaUsina').length > 0) appSuaUsina.init();
	if ($('#propostaFinanciamento').length > 0) appFinanciamento.init();


});