<?php get_header();?>

<header id="header" class="howAquireHeader">

	<?php require_once "inc/topo.php" ?>

	<h2>Mudar é possível</h2>
	
</header><!-- /header -->

<main id="comoContratar">
	<section class="intro">
		<h3 class="title">Energia renovável para você e seus negócios</h3>

	</section>

	
	<section class="steps">
		<ul>
			<li>
				<div class="head">
					<div class="number">01</div>
					<h4 class="title">Orçamento</h4>
				</div>
				<div class="text">
					<div class="line"><div></div></div>
					<div class="paragraph"><p>Solicite seu orçamento para aquisição de uma Usina Solar por meio do nosso site ou através de um dos nossos telefones, serão solicitadas algumas informações sobre seu perfil de consumo. Estas informações de perfil encontram-se definidas na sua própria conta de energia.</p>
					</div>
				</div>
			</li>


			<li>
				<div class="head">
					<div class="number">02</div>
					<h4 class="title">Projeto</h4>
				</div>
				<div class="text">
					<div class="line"><div></div></div>
					<div class="paragraph"><p>Nossa equipe de projeto irá dimensionar o tamanho da unidade geradora de energia e agendará com o cliente uma data para apresentar o projeto, além de verificar necessidades específicas.</p>
					</div>
				</div>
			</li>

			<li>
				<div class="head">
					<div class="number">03</div>
					<h4 class="title">Formas de pagamento</h4>
				</div>
				<div class="text">
					<div class="line"><div></div></div>
					<div class="paragraph"><p>Caso necessário, apresentaremos condições de financiamento bancário existentes no mercado.</p>
					</div>
				</div>
			</li>

			<li>
				<div class="head">
					<div class="number">04</div>
					<h4 class="title">Regulamentação com a concessionária</h4>
				</div>
				<div class="text">
					<div class="line"><div></div></div>
					<div class="paragraph"><p>Tendo o cliente decidido por adquirir o sistema de geração de Energia Solar SWE nós da SWE, ficaremos responsáveis pela regulamentação junto à concessionária de energia elétrica da Usina Solar a ser instalada e adesão ao Sistema de Geração Distribuída.</p>
					</div>
				</div>
			</li>

			<li>
				<div class="head">
					<div class="number">05</div>
					<h4 class="title">Instalação e manutenção</h4>
				</div>
				<div class="text">
					<div class="line"><div></div></div>
					<div class="paragraph"><p>Ficaremos responsáveis também pela instalação do sistema e acompanhamento de geração de energia elétrica. Durante o primeiro ano, oferecemos gratuitamente duas manutenções preventivas na Usina Solar, de forma a assegurar que a mesma opere com máxima eficiência.</p>
					</div>
				</div>
			</li>

			

		</ul>
	</section>

	<?php require_once "inc/pre-footer.php"; ?>
</main>

<?php get_footer(); ?>