<?php get_header();?>


<header id="header" class="homeHeader">

	<?php require_once "inc/topo.php" ?>

	<h2>Mais energia para<br>um futuro sustentável</h2>
	<a class="button btProposal">Solicite uma proposta</a>
	
</header><!-- /header -->

<main id="home">

	<section id="proposta">
		<!--h3 class="title">Receba na hora uma proposta<br>para financiamento. 100% digital.</h3-->
		<h3 class="title">Receba uma proposta<br>para financiamento bancário.</h3>
		<p class="subtitle">Faça um orçamento online e receba uma proposta<br>necessária para iniciar seu processo de financiamento bancário.</p>

		<ul class="etapas">
			<li><span><img src="<?php echo URLTEMA ?>/images/icoConsumo.png"></span><p>Informe seu consumo de energia</p></li>
			<li class="arrow"></li>

			<li><span><img src="<?php echo URLTEMA ?>/images/icoDados.png"></span><p>Insira seus dados cadastrais</p></li>
			<li class="arrow"></li>

			<li><span><img src="<?php echo URLTEMA ?>/images/icoPropEmail.png"></span><p>Receba a proposta do seu projeto</p></li>
		</ul>

		<a class="button btProposal">Solicite uma proposta</a>

		<ul class="financeiras">
			<li>Financiamento</li>
			<li><?php require_once "images/bnb.php" ?></li>
			<li><?php require_once "images/bndes.php" ?></li>
			<li><?php require_once "images/bb.php" ?></li>
			<li><?php require_once "images/caixa.php" ?></li>
			<li><?php require_once "images/santander.php" ?></li>
			<li><?php require_once "images/sicredi.php" ?></li>

		</ul>
	</section>

	<section class="impacto">
		<div class="image col-7" style="background-image: url(<?php echo URLTEMA ?>/images/vegetacao.jpg)"></div>
		<div class="text col-5">
			<div class="ctn">
				<h4>Impacto que<br>importa</h4>
				<p>Populações estão crescendo. Os recursos não. Precisamos de mais energia limpa, barata, confiável, segura e sustentável.<br><br>

			Nosso planeta já possui excelentes fontes de energia limpa. Após anos de engenharia, descobrimos uma maneira de usá-los em harmonia com a natureza e a sociedade. Energia verde ilimitada e acessível. Nós sabemos como fazer um impacto que importa.</p>
			</div>
		</div>
	</section>

	<section class="sobre">
		<div class="text col-5">

			<div class="ctn">
				<h4>Usina Solar<br>SWE</h4>
				<p>Conosco você se beneficia de um futuro melhor em energia.</p>

				<div class="itens">

					<ul>
						<li>
							<strong>Confiabilidade</strong><br>
							Equipamentos de alta qualidade para melhor produção de energia usando uma fonte inesgotável e gratuita.
						</li>
						
						<li>
							<strong>Energia Limpa</strong><br>
							Sem poluição ou qualquer ruído.
						</li>

						<li>
							<strong>Manutenção Mínima</strong><br>
							Sistema estável sem muita necessidade de manutenção e com ótimo custo beneficia.
						</li>

						<li>
							<strong>Instalação Versátil</strong><br>
							A instalação do sistema pode ser realizada tanto em obras em andamento como em construções finalizadas.
						</li>
					</ul>
				</div><!--itens-->

			</div><!--ctn-->
		</div><!--text-->

		<div class="image col-7" style="background-image: url(<?php echo URLTEMA ?>/images/placashome.jpg)"></div>
	</section>

   

</main>


<?php get_footer(); ?>
