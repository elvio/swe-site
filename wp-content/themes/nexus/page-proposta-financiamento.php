<?php get_header();
$chart = $_POST["storeChart"];
$tableEcono = $_POST["storeTable"];
$tableGraph = $_POST["storeTableGraph"];
$precoE = $_POST["precoE"];
$sku = $_POST["sku"];
$fabModulo = $_POST["fabricante_modulo"];
$fabInversor = $_POST["fabricante_inversor"];
$potModulo = $_POST["potencia_modulo"];
$potInversor = $_POST["potencia_inversor"];
$roi = $_POST["roi"];
$roi_anos = $_POST["roi_anos"];
$precoI = ($precoE * .6);

?>

<header id="header" class="pageSimuladorHeader">

	<?php require_once "inc/topo.php" ?>

	<ul class="steps">
		<li><span>1</span><label>Simulador solar</label></li>
		<li><span>2</span><label>Sua Usina Solar SWE</label></li>
		<li class="alpha"><span>3</span><label>Solicitar Proposta para Financiamento</label></li>
	</ul>

	<h2><div><?php echo getHtml('/images/icoPropostaFooter.svg') ?></div>Proposta para Financiamento</h2>
	
</header><!-- /header -->

<main id="propostaFinanciamento">
	<section class="form">
		<?php

		$my_attr = array('chart', 
			'kwp', 
			'modulos',
			'consumo',
			'kwanual',
			'kwmedio',
			'tarifa',
			'economia',
			'roi',
			'roi_anos',
			'tabela_economia',
			'chart_economia',
			'sku',
			'valor_equipamento',
			'valor_instalacao',
			'total',
			'potencia_modulo',
			'potencia_inversor',
			'fabricante_modulo',
			'fabricante_inversor');

		$values = array($chart, 
			$_SESSION['KWP'], 
			$_SESSION['MODULOS'], 
			$_SESSION['CONSUMO'], 
			number_format($_SESSION['KWANUAL'], 2, ',', '.'),
			number_format($_SESSION['KWANUAL']/12, 2, ',', '.'),
			$_SESSION['TARIFA'],
			number_format($_SESSION['TARIFA'] * $_SESSION['CONSUMO'], 2, ',', '.'),
			$roi,
			$roi_anos,
			$tableEcono,
			$tableGraph,
			$sku,
			$precoE,
			$precoI,
			number_format($precoE + $precoI, 2, ',', '.'),
			$potModulo,
			$potInversor,
			$fabModulo,
			$fabInversor);

		for ($i=0; $i<count($my_attr); $i++){
			$attr .= $my_attr[$i] . '="' . $values[$i] . '" '; 
		}


		echo do_shortcode('[contact-form-7 id="48" title="Proposta Financiamento" '.$attr.']'); ?>
	</section>
</main>


<?php get_footer(); ?>