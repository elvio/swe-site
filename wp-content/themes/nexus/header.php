<!doctype html>
<html>
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141802055-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-141802055-1');
</script>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css">
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<link rel="stylesheet" href="<?php echo URLTEMA; ?>/css/style.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name=viewport content="width=device-width">
<meta charset="UTF-8">

<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' );?></title>
<link rel="shortcut icon" href="<?php bloginfo('wpurl');?>/favicon.ico" />

<link rel="preload" as="font" type="font/woff2" 
  href="<?php echo URLTEMA ?>/css/fonts/montserrat-v13-latin-100.woff2" crossorigin>

<link rel="preload" as="font" type="font/woff2" 
  href="<?php echo URLTEMA ?>/css/fonts/montserrat-v13-latin-300.woff2" crossorigin>

<link rel="preload" as="font" type="font/woff2" 
  href="<?php echo URLTEMA ?>/css/fonts/montserrat-v13-latin-500.woff2" crossorigin>

<link rel="preload" as="font" type="font/woff2" 
  href="<?php echo URLTEMA ?>/css/fonts/montserrat-v13-latin-700.woff2" crossorigin>

<?php wp_head(); ?>
</head>


<body class="<?php echo whatsPage(); ?>" onload="start();">
	
	