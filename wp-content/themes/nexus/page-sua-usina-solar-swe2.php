<?php 
session_start();
//var_dump($_SESSION['COORDS']);
if (!isset($_SESSION['COORDS'])) {
  header('Location: ' . SITEURL . '/simulador-energia-solar/');
  wp_die();
} 

get_header();

	$consumo = $_SESSION['CONSUMO'];
	$coords = $_SESSION['COORDS'];
	
	$media = getMediaKwp($coords[0]);
	$potencia = 0.35;

	//FATOR MULTIPLICADOR
	$Fp = $consumo/$media[0];
	//echo $consumo . "<br>";

	//TAMANHO DO SISTEMA
	$Ts = 0.7 * $Fp;
	$Ts = number_format($Ts, 2, '.', '');
	//echo $Ts . "<br>";

	//QUANTIDADE DE MÓDULOS
	$Qr = ceil($Ts/$potencia);
	$Qr = !($Qr % 2) ? $Qr : $Qr + 1;
	//echo $Qr . "<br>";

	$area = $Qr * get_field('area_do_modulo', 'options') * 1.1;

	$peso = $Qr * get_field('peso_unitario', 'options');

	$faixaAnterior = 0;
	//VERIFICA A FAIXA E PRECO POR FAIXA
	while ( have_rows('param_preco', 'options') ) : the_row();
        $kwp .= get_sub_field('kwp_total') . ",";
        $valor .= get_sub_field('kwp_value') . ",";
       
		if ($Ts > $faixaAnterior && $Ts <= get_sub_field('kwp_total')){
			$preco = number_format($Ts, 2, '.', '') * get_sub_field('kwp_value') * 1.6;
			break;
		}
		$faixaAnterior = get_sub_field('kwp_total');
    endwhile;

    $parcelas = 60;

    //FINANCIAMENTO
    $taxa = 1.2/100;
    
    $CF = pow(1 + $taxa, $parcelas);
    $CF = 1/$CF;
    $CF = 1 - $CF;
    $CF = $taxa/$CF;
    //echo $CF;

    $parcela = $preco * $CF;
    $financiamento = $parcela * $parcelas;

    $roi = ceil($financiamento/600);

    //GRAPH
    $months = array();
    foreach ($gradekWh[$media[1]] as $key => $value) {
    	$kwhMes = $value * $Fp;
    	array_push($months,  number_format($kwhMes, 2, '.', ''));
    }    

    $kwp = substr($kwp, 0, -1);
    $valor = substr($valor, 0, -1);

    while ( have_rows('lista_de_produtos', 'options') ) : the_row();
        $kwp_faixa .= get_sub_field('faixa_kwp') . ",";
        $sku .= "'" . get_sub_field('sku') . "',";
        $recomendado .= "'" . get_sub_field('recomendado') . "',";
    endwhile;

    $kwp_faixa = substr($kwp_faixa, 0, -1);
    $sku = substr($sku, 0, -1);
    $recomendado = substr($recomendado, 0, -1);

?>

<header id="header" class="pageSimuladorHeader">

	<?php require_once "inc/topo.php" ?>

	<ul class="steps">
		<li><span>1</span><label>Simulador solar</label></li>
		<li class="alpha"><span>2</span><label>Sua Usina Solar SWE</label></li>
		<li><span>3</span><label>Solicitar Proposta para Financiamento</label></li>
	</ul>

	<h2><div><?php echo getHtml('/images/icoPlaca.svg') ?></div>Sua Usina Solar SWE</h2>
	
</header><!-- /header -->

<main id="suaUsina">
	<div class="float-finance">
		<div class="ctn">
			<div class="info financiamento">
				<label>Financiamento Bancário</label>
				<p class="update"><?php echo $parcelas ?> x de R$ <?php echo number_format($parcela, 2, ',', '.') ?>*</p>
				<span>Valor total <?php echo number_format($financiamento, 2, ',', '.') ?></span>
			</div>
			<a href="<?php echo SITEURL ?>/proposta-financiamento/">
				quero uma proposta para financiamento
			</a>
		</div><!--ctn-->
		<div class="legal">
			* Valores são uma simulação e não representam a nenhuma instituição financeira.
		</div>
	</div>

	<section class="detalhes" >
		<div class="ctn">
			<div class="first">
				<div class="valor col-4">
					<span>Valor</span><br>
					<p><span class="suf">R$</span> <span class="update"><?php echo number_format($preco, 2, ',', '.') ?></span></p>
				</div>

				<div class="P_pv col-4">
					<span>Potência Instalada</span><br>
					<p><span class="update"><?php echo $Ts ?></span><span class="suf"> kWp</span></p>
				</div>

				<div class="area col-4">
					<span>Área mínima necessária</span><br>
					<p><span class="update"><?php echo $area ?></span><span class="suf"> m<sup>2</sup></span></p>
				</div>

				<div class="peso col-4">
					<span>Peso médio</span><br>
					<p><span class="update"><?php echo $peso ?></span><span class="suf"> kg/m<sup>2</sup></span></p>
				</div>

				<div class="M_Total col-4">
					<span>Quantidade de paineis</span><br>
					<p><span class="update"><?php echo $Qr ?></span><span class="suf"> de 350 Watts</span></p>
				</div>

				<div class="roi col-4">
					<span>Retorno do investimento</span><br>
					<p><span class="update"><?php echo $roi ?></span><span class="suf"> meses</span></p>
				</div>

				<br clear="all">
			</div>

		</div>
	</section>

	<section class="produtos">
		<h2 class="title">Equipamentos sugeridos para sua Usina Solar</h2>
		<div class="parent"><div class="update"><br clear="all"></div></div>
	</section>
	<section class="gChart" >
		<div class="ctn">
			<div class="sec">
					<div class="chart">
						<span>Produção Anual de energia: <span class="update"><?php echo number_format(array_sum($months), 2, ',', '.') ?></span> kWh/ano</span>
						<div id="chart_div"></div>
					</div>
					<br clear="all">
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
	var Wp_STC = <?php echo get_field('Wp_STC', 'options');?>;
	var F_mm = getCookie("consumo");
	var F_min = <?php echo get_field('F_min', 'options');?>;
	var Ec = F_mm - F_min;
	var Ecd = Ec/30;
	var P_AC = Ecd/<?php echo get_field('HSP', 'options');?>;
	var P_pv = 100 * P_AC/<?php echo get_field('n_euro', 'options');?>;
	var Wp = Wp_STC + <?php echo get_field('Var_Wp', 'options');?> * <?php echo get_field('T_amb', 'options');?>;
	var M_Total = Math.ceil(1000 * P_pv/Wp);
	var Kw_P_Total = M_Total * Wp_STC;
	var Kw_F_Total = Kw_P_Total/0.8;
	var Qtd_M_Total = Math.ceil(Kw_F_Total/Wp_STC);
	var area = Qtd_M_Total * <?php echo get_field('area_do_modulo', 'options');?>;
	var peso = Qtd_M_Total * <?php echo get_field('peso_unitario', 'options');?>;

	var kwpFaixa = [<?php echo $kwp ?>];
	var preco = [<?php echo $valor ?>];
	var precoFinal = 0;

	var faixaAnterior = 0;

	for (i=0; i<kwpFaixa.length; i++){
		if (P_pv > faixaAnterior && P_pv <= kwpFaixa[i]){
			precoFinal = (P_pv.toFixed(2) * preco[i]) * 1.6;
		} 
		faixaAnterior = kwpFaixa[i];
	}

	//ENCONTRA OS PRODUTOS NO RANGE DE POTÊNCIA
	var kwpFaixaProduct = [<?php echo $kwp_faixa ?>];
	var arrSKU = [<?php echo $sku ?>];
	var recomendado = [<?php echo $recomendado ?>];
	
	//CRIA UM ARRAY BIDIMENSIONAL E ORDENA
	var biArray = [];
	for (n=0; n<kwpFaixaProduct.length; n++){
		biArray.push([ kwpFaixaProduct[n], arrSKU[n], recomendado[n] ]);
	}
	biArray.sort(function(a,b) {
	    return a[0]-b[0]
	});
	
	//console.log(biArray);
	var max = 0;
	var start = 0;

	//RETORNA ARRAY COM SKUs
	function range(getArray, start, end) {
	    var ans = [];

	    for (i = start; i < getArray.length; i++){
		    if (max == getArray[i][0]){
				ans.push([ getArray[i][1], getArray[i][2] ])
			} 
			if(getArray[i][0] > max) break;
	    }

	    return ans;
	}
	faixaAnterior = 0;

	//DEFINE O MAIOR VALOR PARA FAIXA DE POTÊNCIA
	for (z = 0; z < biArray.length; z++){
		//console.log(P_pv > faixaAnterior && P_pv <= biArray[z][0])
		if (P_pv > faixaAnterior && P_pv <= biArray[z][0]) {
			max = biArray[z][0];
			//console.log("MAX: "+ max);
			start = z;
			break;
		}
		faixaAnterior = biArray[z][0];
	}

	var get_sku = range(biArray, start, max);

	console.log("F_min: "+  F_mm);
	console.log("Ec: " + Ec);
	console.log("Ecd: " + Ecd);
	console.log("P_AC: " + P_AC);
	console.log("P_pv: " + P_pv);
	console.log("Wp: " + Wp);
	console.log("M_Total: " + M_Total);
	console.log("Mod Pespectiva: " + Kw_P_Total);
	console.log("Mod Totais: " + Kw_F_Total);
	console.log("QTd Final: " + Qtd_M_Total);
	console.log(kwpFaixa);

	function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "kW", { role: "style" }],
        ["Jan", <?php echo $months[0] ?>, "gold"],
        ["Fev", <?php echo $months[1] ?>, "gold"],
        ["Mar", <?php echo $months[2] ?>, "gold"],
        ["Abr", <?php echo $months[3] ?>, "gold"],
        ["Mai", <?php echo $months[4] ?>, "gold"],
        ["Jun", <?php echo $months[5] ?>, "gold"],
        ["Jul", <?php echo $months[6] ?>, "gold"],
        ["Ago", <?php echo $months[7] ?>, "gold"],
        ["Set", <?php echo $months[8] ?>, "gold"],
        ["Out", <?php echo $months[9] ?>, "gold"],
        ["Nov", <?php echo $months[10] ?>, "gold"],
        ["Dez", <?php echo $months[11] ?>, "gold"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var w = (isMob()) ? 320 : 1000;
      var options = {
        width: w,
        height: 500,
        chartArea: {left:'5%',top:'10%', 'width': '80%', 'height': '80%'},
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        vAxis: {minValue: 0}
      };

      if(isMob()){
      	var chart = new google.visualization.BarChart(document.getElementById("chart_div"));
      } else {
      	var chart = new google.visualization.ColumnChart(document.getElementById("chart_div"));
      }
      
      chart.draw(view, options);
  }

function fcFinanciamento(precoFinal, precoProduto){
	var precoTotal = parseFloat(precoFinal) + parseFloat(precoProduto);
	var financiamento = (precoTotal/50);
	//financiamento += (precoTotal/50);
	financiamento = financiamento.toFixed(2);
	//var precoTotal = (precoProduto * 100 + precoFinal * 100) / 100 
	return financiamento;
}

$(document).ready(function(e) {
	//$('.update', '.P_pv').text(P_pv.toFixed(2));
	//$('.update', '.M_Total').text(Qtd_M_Total + " de ");
	//$('.update', '.area').text(area.toFixed(2));
	//$('.update', '.peso').text(peso);
	//$('.update', '.valor').text(formatMoney(precoFinal, 2, ",", "."));
	//$('.update', '.roi').text(Math.ceil(precoFinal/600));

	// ======================================

	$.ajax({
		type: "POST",
		url: "<?php echo SITEURL ?>/wp-admin/admin-ajax.php",
		data: { sku: get_sku,
				action : 'relatedProducts'},

		success: function(data)
		{
			obj = JSON.parse(data);
			
			if (obj.success==1){
				$('.update','.produtos').prepend(obj.html);
				$("input:radio[name='rdEquipamento']").click(function(event) {
					$('.update', '.financiamento').addClass('reload');
					$('.update', '.financiamento').text("R$");

					$(".ativo").toggleClass('ativo');
					$(this).parents('.col-4').addClass('ativo');

					precoE = $("input:radio[name='rdEquipamento']", ".ativo").val();
					parcela = fcFinanciamento(precoI, precoE);
					sku = $("input:hidden[name='is_sku']", ".ativo").val();
					setCookie("sku", sku, 1);

					var valorTotal = parcela * 50;

					setTimeout(function(){ 
						//$('.update', '.financiamento').removeClass('reload');
						//$('.update', '.financiamento').text("50 x de R$ " + formatMoney(parcela, 2, ",", "."));
						//$('span', '.financiamento').text("Valor total R$ " + formatMoney(valorTotal, 2, ",", "."));

					}, 1500);
					
				});
				
				precoE = $("input:radio[name='rdEquipamento']", ".ativo").val();
				precoI = precoFinal;
				var parcela = fcFinanciamento(precoI, precoE);
				var valorTotal = parcela * 50;
				sku = $("input:hidden[name='is_sku']", ".ativo").val();
				setCookie("sku", sku, 1);

				
				//$('.update', '.financiamento').text("50 x de R$ " + formatMoney(parcela, 2, ",", "."));
				//$('span', '.financiamento').text("Valor total R$ " + formatMoney(valorTotal, 2, ",", "."));
			} else {
				$('.update','.produtos').prepend(obj.html);
			}
			
		}
	});
	// ======================================

	$(window).scroll(function(event){
	  didScroll = true;
	});

	setInterval(function() {
	  if (didScroll && !isMob()) {
	    hasScrolled();
	    didScroll = false;
	  }
	}, 250);
});
$(window).load(function() {
	google.charts.load("current", {packages:['corechart'], 'language': 'pt-BR'});
    google.charts.setOnLoadCallback(drawChart);
});

function hasScrolled() {
	if (!isMob()){
	  var scrollHeight = $(document).height();
		var scrollPosition =  $(window).scrollTop();
		
		var fim = $(document).height() - $(window).height();//$('.float-finance').height() + 640;
		var dif = fim - ( $('footer').height());

		//console.dir(scrollPosition + " : " + dif + " : " + fim);
		if (scrollPosition >= 600 && scrollPosition < dif) {
			$( ".float-finance" ).animate({
			    bottom: "0"
			  }, 225, function() {
			  	
			  		$( ".float-finance" ).css({
				    	'position':'fixed',
				 	 	'bottom': '0px'
					 });
			  });

			 $( ".float-finance" ).css({
			 	//'position':'fixed'
			// 	'bottom': '0px'
			 });
		} else if (scrollPosition >= dif) {
			$( ".float-finance" ).css({
				'position':'absolute',
				'bottom': (dif-40) * -1
			});
		}
	}
}


</script>