<ul class="contatos">
	<li class="phone"><a href="tel:558531203697"><span><img src="<?php echo URLTEMA ?>/images/icoTelefone.svg" width="12"></span><b>(85) 3120-3697</b></a></li>
	<li class="wapp"><a href="https://api.whatsapp.com/send?phone=5585991597939" target="_blank"><span><img src="<?php echo URLTEMA ?>/images/icoWapp.svg" width="14"></span><b>(85) 9159-7939</b></a></li>
	<li class="email btContato"><a ><span><img src="<?php echo URLTEMA ?>/images/icoEmail.svg" width="14"></span><b>Contato</b></a></li>
</ul>
<nav class="navMobile"><div class="close" style="display: none"><?php echo getHtml('/images/close.svg') ?></div><?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?></nav>
<div class="top">
	<div class="left">
		<a href="<?php echo SITEURL ?>"><h1>SWE Energia Solar</h1></a>
		<div class="menuMobile"><img src="<?php echo URLTEMA ?>/images/menuhamb.svg"></div>
		<nav><?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?></nav>
	</div>

	<ul class="actions">
		<!--li><a href=""><span><img src="<?php echo URLTEMA ?>/images/icoProposta.svg" width="23"></span><p>Proposta</p></a></li-->
	</ul>
</div>