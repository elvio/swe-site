<div class="modal-proposta modal" style="display: none">
	<div class="box">
		<div class="close" ><img src="<?php echo URLTEMA ?>/images/close.svg" width="20"></div>
	<?php
	echo do_shortcode('[contact-form-7 id="16" title="Proposta"]');
	?>
	</div>
</div>

<div class="modal-contato modal" style="display: none">
	<div class="box">
		<div class="close"><img src="<?php echo URLTEMA ?>/images/close.svg" width="20"></div>
	<?php
	echo do_shortcode('[contact-form-7 id="24" title="Contato"]');
	?>
	</div>
</div>