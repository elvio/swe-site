<section class="prefooter">
	<h2 class="title">Em que podemos te ajudar?</h2>

	<ul>
		<li class="btProposal"><span class="ico"><?php echo getHtml('/images/icoPropostaFooter.svg') ?></span><br><span class="label">Solicite uma<br>Proposta</span></li>

		<!--li><span class="ico modulo"><?php echo getHtml('/images/icoPlacaFooter.svg') ?></span><br><span class="label">Simulador<br>de Usina SWE</span></li-->

		<li><a href="https://api.whatsapp.com/send?phone=5585991597939" target="_blank"><span class="ico"><?php echo getHtml('/images/icoWappFooter.svg') ?></span><br><span class="label">Fale Conosco<br>por WhatsApp</span></a></li>

		<!--li><span class="ico"><?php echo getHtml('/images/icoPhoneFooter.svg') ?></span><br><span class="label">Contato<br>Por Telefone</span></li-->

		<li><span class="ico btContato"><?php echo getHtml('/images/icoEmailFooter.svg') ?></span><br><span class="label">Envie uma<br>Mensagem</span></li>

		
	</ul>
</section>