<?php 
session_start();

if (!isset($_SESSION['COORDS'])) {
	?>
	<meta http-equiv="refresh" content="0; url=<?php echo SITEURL . '/simulador-energia-solar/' ?>">
	<?php
} 

get_header();

	$consumo = $_SESSION['CONSUMO'];
	$coords = $_SESSION['COORDS'];
	
	$media = getMediaKwp($coords[0]);
	$potencia = 0.35;

	//FATOR MULTIPLICADOR
	$Fp = $consumo/$media[0];
	//echo $consumo . "<br>";

	//TAMANHO DO SISTEMA
	$Ts = 0.7 * $Fp;
	$Ts = number_format($Ts, 2, '.', '');
	//echo $Ts . "<br>";

	//QUANTIDADE DE MÓDULOS
	$Qr = ceil($Ts/$potencia);
	$Qr = !($Qr % 2) ? $Qr : $Qr + 1;
	//echo $Qr . "<br>";

	$area = $Qr * get_field('area_do_modulo', 'options') * 1.1;

	$peso = $Qr * get_field('peso_unitario', 'options');

	$faixaAnterior = 0;
	//VERIFICA A FAIXA E PRECO POR FAIXA
	while ( have_rows('param_preco', 'options') ) : the_row();
        $kwp .= get_sub_field('kwp_total') . ",";
        $valor .= get_sub_field('kwp_value') . ",";
       
		if ($Ts > $faixaAnterior && $Ts <= get_sub_field('kwp_total')){
			$preco = (number_format($Ts, 2, '.', '') * get_sub_field('kwp_value')) * 1.6;
			break;
		}
		$faixaAnterior = get_sub_field('kwp_total');
    endwhile;

	//FINANCIAMENTO
    $parcelas = 60;
    $taxa = 1.2/100;
    
    $CF = pow(1 + $taxa, $parcelas);
    $CF = 1/$CF;
    $CF = 1 - $CF;
    $CF = $taxa/$CF;
    //echo $CF;

    $parcela = $preco * $CF;
    $financiamento = $parcela * $parcelas;

    //GRAPH
    $months = array();
    foreach ($gradekWh[$media[1]] as $key => $value) {
    	$kwhMes = $value * $Fp;
    	array_push($months,  number_format($kwhMes, 2, '.', ''));
    }    

    $kwp = substr($kwp, 0, -1);
    $valor = substr($valor, 0, -1);

    while ( have_rows('lista_de_produtos', 'options') ) : the_row();
        $kwp_faixa .= get_sub_field('faixa_kwp') . ",";
        $sku .= "'" . get_sub_field('sku') . "',";
        $recomendado .= "'" . get_sub_field('recomendado') . "',";
    endwhile;

    $acumulado = 0;
    $m = 0;
    $roi = 0;
    $tarifa = ($_SESSION['INSTALACAO'] == "Área Rural") ? get_field('valor_da_tarifa_rural', 'options') : get_field('valor_da_tarifa_baixa', 'options');
    while ($acumulado < $preco){
    	$acumulado += $months[$m] * $tarifa;
    	$roi++;
    	if($m < count($months)) $m++;
    	else $m = 0;
    }

    $kwp_faixa = substr($kwp_faixa, 0, -1);
    $sku = substr($sku, 0, -1);
    $recomendado = substr($recomendado, 0, -1);

    $economia = array_sum($months) * $tarifa;
    $str_preco = number_format(abs($preco), 2, ',', '.');
    $str_economia = number_format(abs($economia), 2, ',', '.');
    $saldo = $economia - $preco;

    //SESSION VARS PARA GERAÇÃO DO PDF
    $_SESSION['KWP'] = $Ts;
    $_SESSION['MODULOS'] = $Qr;
    $_SESSION['KWANUAL'] = array_sum($months);
    $_SESSION['TARIFA'] = $tarifa;
    //$_SESSION['ROI'] = $roi;
    //$roi = number_format($roi/12, 1, ',', '.');
    //$_SESSION['ROI_ANOS'] = $roi;

?>

<header id="header" class="pageSimuladorHeader">

	<?php require_once "inc/topo.php" ?>

	<ul class="steps">
		<li><span>1</span><label>Simulador solar</label></li>
		<li class="alpha"><span>2</span><label>Sua Usina Solar SWE</label></li>
		<li><span>3</span><label>Solicitar Proposta para Financiamento</label></li>
	</ul>

	<h2><div><?php echo getHtml('/images/icoPlaca.svg') ?></div>Sua Usina Solar SWE</h2>
	
</header><!-- /header -->

<main id="suaUsina">
	<div class="float-finance" >
		<div class="ctn">
			<div class="info financiamento">
				<label>Financiamento Bancário</label>
				<p class="update"><?php echo $parcelas ?> x de R$ <?php echo number_format($parcela, 2, ',', '.') ?>*</p>
				<span>Valor total <?php echo number_format($financiamento, 2, ',', '.') ?></span>
			</div>
			<form action="<?php echo SITEURL ?>/proposta-financiamento/" method="post">
				<input type="hidden" name="storeChart" id="image">
				<input type="hidden" name="storeTable" id="table">
				<input type="hidden" name="storeTableGraph" id="tableGraph">
				<input type="hidden" name="sku" id="sku" >
				<input type="hidden" name="roi" id="roi" >
				<input type="hidden" name="roi_anos" id="roi_anos" >
				<input type="hidden" name="precoE" id="precoE" >
				<input type="hidden" name="fabricante_modulo" id="fabModulo" >
				<input type="hidden" name="fabricante_inversor" id="fabInversor" >
				<input type="hidden" name="potencia_modulo" id="potModulo" >
				<input type="hidden" name="potencia_inversor" id="potInversor" >
				<input type="submit" id="btnFinanciamento" value="QUERO UMA PROPOSTA PARA FINANCIAMENTO">
			</form>
			
		</div><!--ctn-->
		<div class="legal">
			* Valores são uma simulação e não representam a nenhuma instituição financeira.
		</div>
	</div>
	<section class="detalhes" >
		<div class="ctn">
			<div class="first">
				<div class="valor col-4">
					<span>Equipamentos + Instalação</span><br>
					<p><span class="suf">R$</span> <span class="update"></span></p>
				</div>

				<div class="P_pv col-4">
					<span>Potência Instalada</span><br>
					<p><span class="update"><?php echo $Ts ?></span><span class="suf"> kWp</span></p>
				</div>

				<div class="area col-4">
					<span>Área mínima necessária</span><br>
					<p><span class="update"><?php echo $area ?></span><span class="suf"> m<sup>2</sup></span></p>
				</div>

				<div class="peso col-4">
					<span>Peso médio</span><br>
					<p><span class="update"><?php echo $peso ?></span><span class="suf"> kg/m<sup>2</sup></span></p>
				</div>

				<div class="M_Total col-4">
					<span>Quantidade de paineis</span><br>
					<p><span class="update"><?php echo $Qr ?></span><span class="suf"> de 350 Watts</span></p>
				</div>

				<div class="roi col-4">
					<span>Retorno do investimento</span><br>
					<p><span class="update"></span><span class="suf"> anos</span></p>
				</div>

				<br clear="all">
			</div>

		</div>
	</section>

	<section class="gChart" >
		<div class="ctn">
			<div class="sec">
					<div class="chart">
						<span>Produção Anual de energia: <span class="update"><?php echo number_format(array_sum($months), 2, ',', '.') ?></span>kWh/ano</span>
						<div id="chart_div"></div>
					</div>
					<br clear="all">
			</div>
		</div>
	</section>

	<section class="economia">
		<span>Retorno sobre o investimento</span>
		<div class="table">
			<ul class="header">
				<li>ANO</li>
				<li>CUSTO</li>
				<li>ECONOMIA</li>
				<li>SALDO</li><!--/tr-->
			</ul>
			<ul class="ctn">
				
			</ul>
		</div>

		<div class="chartTable">
			<div class="ctn">
				<div class="sec">
					<div class="chart">
						<div id="chart_table"></div>
					</div>
					<br clear="all">
				</div>
			</div>
		</div>
	</section>

	<section class="produtos">
		<h2 class="title">Equipamentos sugeridos para sua Usina Solar</h2>
		<div class="parent"><div class="update"><br clear="all"></div></div>
	</section>

</main>
<div id="hiddenChart" style="display: none"></div>
<div id="hiddenChartTable" style="display: none"></div>


<?php get_footer(); ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
function fcFinanciamento(precoProduto){
	var precoFinal = precoProduto * 1.6;
	var parcelas = 60;
    var taxa = 1.2/100;
    
    CF = Math.pow(1 + taxa, parcelas);
    CF = 1/CF;
    CF = 1 - CF;
    CF = taxa/CF;
    //echo $CF;

    parcela = precoFinal * CF;
    financiamento = parcela * parcelas;

	return [parcela, financiamento, precoFinal];
}

	function drawChart() 
	{
      var data = google.visualization.arrayToDataTable([
        ["Element", "kW", { role: "style" }],
        ["Jan", <?php echo $months[0] ?>, "gold"],
        ["Fev", <?php echo $months[1] ?>, "gold"],
        ["Mar", <?php echo $months[2] ?>, "gold"],
        ["Abr", <?php echo $months[3] ?>, "gold"],
        ["Mai", <?php echo $months[4] ?>, "gold"],
        ["Jun", <?php echo $months[5] ?>, "gold"],
        ["Jul", <?php echo $months[6] ?>, "gold"],
        ["Ago", <?php echo $months[7] ?>, "gold"],
        ["Set", <?php echo $months[8] ?>, "gold"],
        ["Out", <?php echo $months[9] ?>, "gold"],
        ["Nov", <?php echo $months[10] ?>, "gold"],
        ["Dez", <?php echo $months[11] ?>, "gold"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var w = (isMob()) ? 320 : 1000;
      var leftP = (isMob()) ? '10%' : '5%';
      var options = {
        width: w,
        height: 500,
        chartArea: {left:leftP,top:'10%', 'width': '80%', 'height': '80%'},
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        vAxis: {minValue: 0},
        hAxis: {minValue: 0}
      };

      tempChart = new google.visualization.ColumnChart(document.getElementById("hiddenChart"));

      if(isMob()){
      	var chart = new google.visualization.BarChart(document.getElementById("chart_div"));
      } else {
      	var chart = new google.visualization.ColumnChart(document.getElementById("chart_div"));
      }

      google.visualization.events.addListener(tempChart, 'ready', function () {
      	$('#image').val(tempChart.getImageURI());
      });
      
      
      chart.draw(view, options);
      tempChart.draw(view, options);
  }

  function drawChartTable(saldo) {
  	var elem = [];
  	var color = [];
	
	for (var i=0; i<saldo.length; i++){
  		var ano = i + 1;
  		var cor = (saldo[i] < 0) ? "#ca0000" : "#0d9546";
  		color.push(cor);
  		elem.push(saldo[i])
  		//elem.push(["Ano "+ano, saldo[i], color]);
  		// data.addRow("Ano "+ano, saldo[i], color);
  	}
  

      var data = google.visualization.arrayToDataTable([
      	["Element", "R$", { role: "style" }],
      	["Ano 1", elem[0], color[0]],
      	["Ano 2", elem[1], color[1]],
      	["Ano 3", elem[2], color[2]],
      	["Ano 4", elem[3], color[3]],
      	["Ano 5", elem[4], color[4]],
      	["Ano 6", elem[5], color[5]],
      	["Ano 7", elem[6], color[6]],
      	["Ano 8", elem[7], color[7]],
      	["Ano 9", elem[8], color[8]],
      	["Ano 10", elem[9], color[9]],

      	]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var w = (isMob()) ? 320 : 890;
      var leftP = (isMob()) ? '10%' : '10%';
      var options = {
        width: w,
        height: 500,
        chartArea: {left:leftP,top:'10%', 'width': '85%', 'height': '80%'},
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        vAxis: {minValue: 0},
        hAxis: {minValue: 0}
      };

      tempChart = new google.visualization.ColumnChart(document.getElementById("hiddenChart"));

      if(isMob()){
      	var chart = new google.visualization.BarChart(document.getElementById("chart_table"));
      } else {
      	var chart = new google.visualization.ColumnChart(document.getElementById("chart_table"));
      }

      google.visualization.events.addListener(tempChart, 'ready', function () {
      	$('#tableGraph').val(tempChart.getImageURI());
      });
      
      
      chart.draw(view, options);
      tempChart.draw(view, options);
  }

function numeroParaMoeda(n, c, d, t)
{
    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}


function createTable(price){
	var  economia = <?php echo array_sum($months) * $tarifa ?>;
	var roi_anos = price/economia;
	roi_anos = roi_anos.toFixed(1);

	var saldo = economia - price;
	var status = (saldo > 0) ? "green" : "red";
	var li = '<!--tr--><li>1</li>';
	li += '<li class="red">'+ numeroParaMoeda(price) +'</li>';
	li += '<li class="green">'+numeroParaMoeda(economia)+'</li>';
	li += '<li class="'+status+'">'+ numeroParaMoeda(saldo) +'</li><!--/tr-->';

	arr_saldo = [saldo];

	for (var i=2; i<11; i++)
	{
		economia += economia * 0.0105;
		str_custo = numeroParaMoeda(Math.abs(saldo));
		str_economia = numeroParaMoeda(economia);

		if(saldo > 0){
			str_custo = 0;
			saldo += economia;

		} else {
			saldo = economia - Math.abs(saldo);
		}

		arr_saldo.push(saldo);

		li += '<!--tr--><li>'+i+'</li>';
		li += '<li class="'+status+'">'+ str_custo +'</li>';
		li += '<li class="green">'+ str_economia +'</li>';
		status = (saldo > 0) ? "green" : "red";
		li += '<li class="'+ status +'">'+ numeroParaMoeda(saldo) + '</li><!--/tr-->';
		
	}

	//$('.table .ctn').html(li);

	//CONVERTE DIV para TABLE PARA ANEXAR AO PDF
	var table = $('.table').html();
	table = table.split(/\r?\n|\r/g).join("");
	table = table.split("	").join("");
	table = table.split('<ul class="header">').join('<table width="100%" cellpadding="10"><tr bgcolor="#eaeaea">');
	table = table.split('<li>').join('<td style="font-family: Helvetica, Arial, sans-serif; width: 25%;list-style: none;text-align: center;padding: 12px 0;border-bottom: 1px solid #eaeaea; font-weight:bold">');
	table = table.split('</li>').join('</td>');
	table = table.split("<!--/tr-->").join("</tr>");
	table = table.split("</ul>").join("</table>");
	table = table.split('<ul class="ctn">').join('<table width="100%" cellpadding="10">');
	table = table.split("<!--tr-->").join("<tr>");
	table = table.split('<li class="red">').join('<td style="font-family: Helvetica, Arial, sans-serif; color:#bb0009; width: 25%;list-style: none;text-align: center;padding: 12px 0;border-bottom: 1px solid #eaeaea;">');
	
	table = table.split('<li class="green">').join('<td style="font-family: Helvetica, Arial, sans-serif; color:#0d9345; width: 25%;list-style: none;text-align: center;padding: 12px 0;border-bottom: 1px solid #eaeaea;">');
	table = table.split('"').join("");
   // $('#table').val( table );

    return [li, table, arr_saldo, roi_anos];

}


$(document).ready(function(e) {
	// ======================================

	$(window).scroll(function(event){
	  didScroll = true;
	});

	setInterval(function() {
	  if (didScroll && !isMob()) {
	    hasScrolled();
	    didScroll = false;
	  }
	}, 250);

    
    /* ======================================= 
    			ENCONTRA PRODUTOS
    =========================================*/

    //ENCONTRA OS PRODUTOS NO RANGE DE POTÊNCIA
	var kwpFaixaProduct = [<?php echo $kwp_faixa ?>];
	var arrSKU = [<?php echo $sku ?>];
	var recomendado = [<?php echo $recomendado ?>];
	var P_pv = <?php echo $Ts ?>
	
	//CRIA UM ARRAY BIDIMENSIONAL E ORDENA DO MENOR PARA O MAIOR
	var biArray = [];
	for (n=0; n<kwpFaixaProduct.length; n++){
		biArray.push([ kwpFaixaProduct[n], arrSKU[n], recomendado[n] ]);
	}
	biArray.sort(function(a,b) {
	    return a[0]-b[0]
	});
	
	//console.log(biArray);
	var max = 0;
	var start = 0;

	//RETORNA ARRAY COM SKUs
	function range(getArray, start, end) {
	    var ans = [];

	    for (i = start; i < getArray.length; i++){
		    if (max == getArray[i][0]){
				ans.push([ getArray[i][1], getArray[i][2] ])
			} 
			if(getArray[i][0] > max) break;
	    }

	    return ans;
	}
	faixaAnterior = 0;

	//DEFINE O MAIOR VALOR PARA FAIXA DE POTÊNCIA
	for (z = 0; z < biArray.length; z++){
		//console.log(P_pv > faixaAnterior && P_pv <= biArray[z][0])
		if (P_pv > faixaAnterior && P_pv <= biArray[z][0]) {
			max = biArray[z][0];
			//console.log("MAX: "+ max);
			start = z;
			break;
		}
		faixaAnterior = biArray[z][0];
	}

	var get_sku = range(biArray, start, max);

	$.ajax({
		type: "POST",
		url: "<?php echo SITEURL ?>/wp-admin/admin-ajax.php",
		data: { sku: get_sku,
				action : 'relatedProducts'},

		success: function(data)
		{
			obj = JSON.parse(data);
			
			if (obj.success==1){
				$('.update','.produtos').prepend(obj.html);
				$("input:radio[name='rdEquipamento']").click(function(event) {
					$('.update', '.financiamento').addClass('reload');
					$('.update', '.financiamento').text("R$");

					$(".ativo").toggleClass('ativo');
					$(this).parents('.col-4').addClass('ativo');

					precoE = $("input:radio[name='rdEquipamento']", ".ativo").val();
					financiamento = fcFinanciamento(precoE);
					sku = $("input:hidden[name='is_sku']", ".ativo").val();

					finTable = createTable(financiamento[2]);
					$('.table .ctn').html(finTable[0]);
					$('#table').val( finTable[1] );
					google.charts.setOnLoadCallback(function(){ drawChartTable(finTable[2]) });
					$('.update', '.roi').text(finTable[3]);

					$('#sku').val(sku);
					$('#precoE').val(precoE);
					$('#roi_anos').val(finTable[3]);
					$('#roi').val((finTable[3]/12)* 10);

					$('#potInversor').val( $('is_potencia_inversor', '.ativo').val() );
					$('#potModulo').val( $('is_potencia_modulo', '.ativo').val() );
					$('#fabInversor').val( $('is_fab_inversor', '.ativo').val() );
					$('#fabModulo').val( $('is_fab_modulo', '.ativo').val() );

					setTimeout(function(){ 
						$('.update', '.financiamento').removeClass('reload');
						$('.update', '.financiamento').text("60 x de R$ " + formatMoney(financiamento[0], 2, ",", "."));
						$('span', '.financiamento').text("Valor total R$ " + formatMoney(financiamento[1], 2, ",", "."));
						$('.update', '.valor').text(formatMoney(financiamento[2], 2, ",", "."));

					}, 1500);
					
				});
				
				//PRECO DO EQUIPAMENTO
				precoE = $("input:radio[name='rdEquipamento']", ".ativo").val();

				var financiamento = fcFinanciamento(precoE);
				
				sku = $("input:hidden[name='is_sku']", ".ativo").val();
				
				$('#sku').val(sku);
				$('#precoE').val(precoE);
				$('#potInversor').val( $("input:hidden[name='is_potencia_inversor']", '.ativo').val() );
				$('#potModulo').val( $("input:hidden[name='is_potencia_modulo']", '.ativo').val() );
				$('#fabInversor').val( $("input:hidden[name='is_fabricante_inversor']", '.ativo').val() );
				$('#fabModulo').val( $("input:hidden[name='is_fabricante_modulo']", '.ativo').val() );

				$('.update', '.financiamento').text("60 x de R$ " + formatMoney(financiamento[0], 2, ",", "."));
				$('span', '.financiamento').text("Valor total R$ " + formatMoney(financiamento[1], 2, ",", "."));
				$('.update', '.valor').text(formatMoney(financiamento[2], 2, ",", "."));

				finTable = createTable(financiamento[2]);
				$('.table .ctn').html(finTable[0]);
				$('#table').val( finTable[1] );
				$('.update', '.roi').text(finTable[3]);
				$('#roi_anos').val(finTable[3]);
				$('#roi').val((finTable[3]/12)* 10);
				
				google.charts.setOnLoadCallback(function(){ drawChartTable(finTable[2]) });


			} else {
				$('.update','.produtos').prepend(obj.html);
			}
			
		}
	});
	// ======================================

});
$(window).load(function() {
	google.charts.load("current", {packages:['corechart'], 'language': 'pt-BR'});
    google.charts.setOnLoadCallback(drawChart);
    //google.charts.setOnLoadCallback(drawChartTable);
});

function hasScrolled() {
	if (!isMob()){
	  var scrollHeight = $(document).height();
		var scrollPosition =  $(window).scrollTop();
		
		var fim = $(document).height() - $(window).height();//$('.float-finance').height() + 640;
		var dif = fim - ( $('footer').height());

		//console.dir(scrollPosition + " : " + dif + " : " + fim);
		if (scrollPosition >= 600 && scrollPosition < dif) {
			$( ".float-finance" ).animate({
			    bottom: "0"
			  }, 225, function() {
			  	
			  		$( ".float-finance" ).css({
				    	'position':'fixed',
				 	 	'bottom': '0px'
					 });
			  });

			 $( ".float-finance" ).css({
			 	//'position':'fixed'
			// 	'bottom': '0px'
			 });
		} else if (scrollPosition >= dif) {
			$( ".float-finance" ).css({
				'position':'absolute',
				'bottom': (dif-40) * -1
			});
		}
	}
}

</script>