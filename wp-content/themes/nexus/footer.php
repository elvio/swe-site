<footer>
	<div class="info">
		<div class="ctn">
			<nav><?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?></nav>

			<div class="end">
				<div><img src="<?php echo URLTEMA ?>/images/marcarodape.png"></div>
				<p>Select Comércio Importação e Exportação Ltda.<br>
				R. Alfeu Aboim, 693B - Papicu, Fortaleza - CE, 60175-375</p>
			</div>
		</div>

		<div class="contato">
			<ul>
				<li><span><?php echo getHtml('/images/icoWapp.svg') ?></span><a href="tel:5585991597939">(85)99159-7939</a></li>
				<li><span><?php echo getHtml('/images/icoTelefone.svg') ?></span><a href="https://api.whatsapp.com/send?phone=558531203697" target="_blank">(85)3120-3697</a></li>
				<li class="btContato"><span><?php echo getHtml('/images/icoEmail.svg') ?></span>CONTATO</li>
			</ul>
		</div>
	</div>
</footer>

<?php require_once "inc/modal-proposta.php"; 
wp_footer();
?>

</body>



<script type="text/javascript">
	function dynamicallyLoadScript(url) {
	    var script = document.createElement("script");  // create a script DOM node
	    script.src = url;  // set its src to the provided URL
	    document.head.appendChild(script);  // add it to the end of the head section of the page (could change 'head' to 'body' to add it to the end of the body section instead)
	}
	function createCSS(url){

	    var link = document.createElement('link');
	    link.rel = 'stylesheet';  
        link.type = 'text/css'; 
        link.href = url;  
        document.head.appendChild(link);

	}

	
	var pathjs = "<?php echo URLTEMA ?>/js/";

	
	function start(){
		//dynamicallyLoadScript('//code.jquery.com/jquery-1.11.0.min.js');
		

		// dynamicallyLoadScript('<?php echo URLTEMA ?>/js/TweenMax.min.js');
		// dynamicallyLoadScript('<?php echo URLTEMA ?>/js/jquery.validate.min.js');
		// dynamicallyLoadScript('<?php echo URLTEMA ?>/js/jquery.mask.js');
		// dynamicallyLoadScript('<?php echo URLTEMA ?>/js/jquery.scrollTo-min.js');
		// dynamicallyLoadScript('<?php echo URLTEMA ?>/js/slick.min.js');
		// dynamicallyLoadScript('<?php echo URLTEMA ?>/js/functions.js');
		setTimeout(function(){ document.getElementById('functions').src = '<?php echo URLTEMA ?>/js/functions.js?<?php echo rand()?>'}, 1000);
	}

</script>

<script src="<?php echo URLTEMA ?>/js/TweenMax.min.js"></script>
<script src="<?php echo URLTEMA ?>/js/jquery.validate.min.js"></script>
<script src="<?php echo URLTEMA ?>/js/jquery.mask.js"></script>
<script src="<?php echo URLTEMA ?>/js/jquery.scrollTo-min.js"></script>
<script defer id="functions"></script>


<script type="text/javascript">
  // WebFontConfig = {
  //   google: { families: [ 'Montserrat:300,500,600,700' ] }
  // };
  // (function() {
  //   var wf = document.createElement('script');
  //   wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
  //   wf.type = 'text/javascript';
  //   wf.async = 'true';
  //   var s = document.getElementsByTagName('script')[0];
  //   s.parentNode.insertBefore(wf, s);
  // })(); 
</script>

<!--script src="<?php echo URLTEMA; ?>/js/main.js"></script-->
</html>