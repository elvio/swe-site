<?php get_header();?>

<header id="header" class="financeHeader">

	<?php require_once "inc/topo.php" ?>

	<h2>Financiamento para Energia Solar</h2>
	
</header><!-- /header -->

<main id="financiamento">
	<section class="intro">
		<h3 class="title">Acredite nos  seus objetivos</h3>
		<p>Para viabilizar e incentivar a geração de energia por sistema fotovoltaico, diversos bancos criaram uma linha especial de financiamento para você quem acredita que pode ser mais sustentável.</p>

	</section>

	<section class="icons">
		<ul class="">
			<li><span><?php echo getHtml('/images/icoValor.svg') ?></span><p>Financiamento de 100% do valor do equipamento e da instalação.</p></li>
			<li><span><?php echo getHtml('/images/icoAgenda.svg') ?></span><p>Até 12 anos para pagar.</p></li>
			<li><span><?php echo getHtml('/images/icoAbaco.svg') ?></span><p>A taxa de juros a partir de 6,5% ao ano</p></li>
		</ul>
	</section>

	<section class="bancos">
		<h4 class="title">Bancos com linhas de crédito</h4>

		<ul>
			<li class="banco">
				<div><?php require_once "images/bb.php" ?></div>
				<h4>Banco do Brasil</h4>
			</li>

			<li class="banco">
				<div><?php require_once "images/bnb.php" ?></div>
				<h4>Banco do Nordeste</h4>
			</li>

			<li class="banco">
				<div><?php require_once "images/bndes.php" ?></div>
				<h4>BNDES</h4>
			</li>

			<li class="banco">
				<div><?php require_once "images/caixa.php" ?></div>
				<h4>Caixa Econômica</h4>
			</li>

			<li class="banco">
				<div><?php require_once "images/santander.php" ?></div>
				<h4>Santander</h4>
			</li>

			<li class="banco">
				<div><?php require_once "images/sicredi.php" ?></div>
				<h4>Sicredi</h4>
			</li>
		</ul>
	</section>


	<section class="passos">
		<h4 class="title">Como contratar</h4>

		<ul>
			<li>
				<div><?php echo getHtml('/images/icoPlaca.svg') ?></div>
				<h4>Descubra o tamanho de Usina Solar</h4>
			</li>

			<li>
				<div><?php echo getHtml('/images/icoProposta.svg') ?></div>
				<h4>Solicite uma proposta a SWE</h4>
			</li>

			<li class="banco">
				<div><?php echo getHtml('/images/icoPropostaDetalhes.svg') ?></div>
				<h4>Leve a proposta da SWE até Banco</h4>
			</li>

		</ul>
	</section>

	<?php require_once "inc/pre-footer.php"; ?>
</main>

<?php get_footer(); ?>