<?php get_header();?>

<header id="header" class="aboutHeader">

	<?php require_once "inc/topo.php" ?>

	<h2>Sobre nós</h2>
	
</header><!-- /header -->

<main id="sobre">
	<section class="intro">
		<p>Fundada em 2008, somos uma empresa que oferece <strong>soluções em energia renonável, consultoria em eficiência energética e gestão de projetos em energias renováveis</strong>. Nossa empresa é vinculada ao <a href="http://www.proelhospitalar.com/site/index.php?lang=pt" target="_blank">Grupo Proel</a>, mas focada em apresentar soluções energéticas de forma coerente e com responsabilidade.  
Atuamos também na venda e manutenção de equipamentos de geração de energia alta qualidade, sempre com a preocupação em adequar os equipamentos às necessidades dos clientes.</p>

<p>A SWE é uma empresa que visa permitir a produção independente de energia elétrica de forma limpa e sustentável e tem como principal missão entregar soluções de engenharia em energia renovável de forma robusta, modular e flexível.</p>

<h3>Missão</h3>

<p>Oferecer soluções para geração de energias renováveis robustas, modulares e flexíveis.</p>

<h3>Visão</h3>

<p>Ser reconhecida como a empresa que contribui para reduzir a deficiência energética de nosso país com o uso de fontes alternativas e sustentáveis de energia.</p>

<h3>Valores</h3>

<ul><li>Ética, transparência e honestidade em suas relações;</li>

<li>Equilíbrio nas relações e ações do dia a dia;</li>
<li>Comunicação rápida, objetiva e eficiente;</li>
<li>Valorização do ser humano;</li>

<li>Pessoalidade nas relações.</li>
</ul>

	</section>

	<section class="fotos">
			<ul class="slider">
			<?php
			$images = get_field('galeria');
			

			foreach( $images as $image ):		
			?>
				<li><figure><img src="<?php echo $image['url'] ?>"></figure></li>
			
			<?php
	       endforeach;
	        ?>

		</ul>
	</section>
<?php 
			//ARTIGOS
			$argsDepoimentos = array(
				'post_type' => 'post_depoimentos',
				'posts_per_page' => -1,
			);

			$queryDepoimentos = new WP_Query( $argsDepoimentos ); 

			if ($queryDepoimentos->have_posts()):

?>
	<section class="depoimentos">
		<h2 class="title">Depoimentos</h2>

		<ul>
			<?php 
			

			 while ($queryDepoimentos->have_posts()) : $queryDepoimentos->the_post();
			?>

			<li class="person">
				<h4><?php the_title() ?></h4>
				<h5><?php echo get_field('empresa') ?></h5>
				<div class="text">
					<?php the_content() ?>
				</div>
			</li>

			<?php
			endwhile; 
			?>
			
		</ul>
	</section>
	<?php
		endif;
	?>

	<?php require_once "inc/pre-footer.php"; ?>
</main>

<?php get_footer(); ?>